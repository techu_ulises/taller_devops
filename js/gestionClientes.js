var banderasObtenidas;

function getClientes() {
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function()
  {
    if (this.readyState == 4 && this.status == 200) {
      console.log(request.responseText);
      banderasObtenidas = request.responseText;
      procesarImagenes();
    }
  }
  request.open("GET", url, true);
  request.send();

  function procesarImagenes() {
    var JSONBanderas = JSON.parse(banderasObtenidas);
    var divTabla = document.getElementById("divClientes");
    var tabla = document.createElement("table");
    tabla.classList.add("table"); //estilos
    tabla.classList.add("table-striped");
    var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

    var cabecera = document.createElement("thead");
    var columnaNombre = document.createElement("th");
    columnaNombre.innerText = "Nombre";
    var columnaCiudad = document.createElement("th");
    columnaCiudad.innerText = "Ciudad";
    var columnaTelefono = document.createElement("th");
    columnaTelefono.innerText = "Teléfono";
    var columnaBandera = document.createElement("th");
    columnaBandera.innerText = "Bandera";
    cabecera.appendChild(columnaNombre);
    cabecera.appendChild(columnaCiudad);
    cabecera.appendChild(columnaTelefono);
    cabecera.appendChild(columnaBandera);
    tabla.appendChild(cabecera);
    var body =  document.createElement("tbody");
    tabla.appendChild(body);
    for (var i=0; i<JSONBanderas.value.length; i++) {

      var nuevaFila = document.createElement("tr");
      var columnaNombre = document.createElement("td");
      columnaNombre.innerText= JSONBanderas.value[i].ContactName;
      var columnaCiudad = document.createElement("td");
      columnaCiudad.innerText= JSONBanderas.value[i].City;
      var columnaTelefono = document.createElement("td");
      columnaTelefono.innerText= JSONBanderas.value[i].Phone;
      var columnaBandera = document.createElement("td");
      var imgBandera = document.createElement("img");
      imgBandera.classList.add("flag"); //estilo de index.css para la bandera
      if (JSONBanderas.value[i].Country=="UK")
      {
          imgBandera.src = rutaBandera + "United-Kingdom.png";
      }
      else {
          imgBandera.src = rutaBandera + JSONBanderas.value[i].Country + ".png";
      }
      //columnaBandera.innerText = imgBandera;
      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaCiudad);
      nuevaFila.appendChild(columnaTelefono);
      columnaBandera.appendChild(imgBandera);
      nuevaFila.appendChild(columnaBandera);
      body.appendChild(nuevaFila);
    }
    tabla.appendChild(body);
    divTabla.appendChild(tabla);

      //console.log(JSONBanderas.value[0]);
  }
}
